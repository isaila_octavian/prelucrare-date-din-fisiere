#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>



void listDir(const char *path)
{
DIR *dir = NULL;
struct dirent *entry = NULL;

dir = opendir(path);
if(dir == NULL) {
perror("Could not open directory");
}
else printf("SUCCESS\n");
while((entry = readdir(dir)) != NULL) {
if(strcmp(entry->d_name, ".") != 0
&& strcmp(entry->d_name, "..") != 0){
printf("%s", path);
printf("/%s\n", entry->d_name);
}
}
closedir(dir);
}

void listDirST(const char *path, const char* startt)
{
DIR *dir = NULL;
struct dirent *entry = NULL;
int n=strlen(startt);

//printf("%s%s", path,startt);

dir = opendir(path);
if(dir == NULL) {
perror("Could not open directory");
}
else printf("SUCCESS\n");
while((entry = readdir(dir)) != NULL) {
if((strcmp(entry->d_name, ".") != 0) && (strcmp(entry->d_name, "..") != 0) 
&& (strncmp(entry->d_name,startt,n)==0)) {
printf("%s", path);
printf("/%s\n", entry->d_name);
}
}
closedir(dir);
}


void listRec(const char *path)
{
    DIR *dir = NULL;
    struct dirent *entry = NULL;
    char fullPath[1024];
    struct stat statbuf;

    dir = opendir(path);
    if(dir == NULL) {
        perror("ERROR\ninvalid directory path\n");
        return;
    }	
    while((entry = readdir(dir)) != NULL) {
        if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(fullPath, 1024, "%s/%s", path, entry->d_name);
            if(lstat(fullPath, &statbuf) == 0) {
                printf("%s\n", fullPath);
                if(S_ISDIR(statbuf.st_mode)) {
                    listRec(fullPath);
                }
            }
        }
    }
    closedir(dir);
}

void hasPermision(const char *path)
{
    DIR *dir = NULL;
    struct dirent *entry = NULL;
    char fullPath[1024];
    struct stat statbuf;

    dir = opendir(path);
    if(dir == NULL) {
        printf("ERROR\ninvalid directory path\n");
        return;
    }   
    while((entry = readdir(dir)) != NULL) {
        if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(fullPath, 1024, "%s/%s", path, entry->d_name);
            if(lstat(fullPath, &statbuf) == 0) {
                //printf("%s\n", fullPath);
                    if((statbuf.st_mode & 0200))
                {
                    printf("%s", path);
                    printf("/%s\n", entry->d_name);
                }
        }
    }
    }
    closedir(dir);
}

void fisierele(const char* path, int decizie)
{

int fd = -1;

fd = open(path, O_RDONLY);
if(fd == -1) {
perror("ERROR\nCould not open input file");
return;
}
else
{
ssize_t size=0;
char magic[5];
//char sfarsit[1]; 
int heade_size=0;
int version=0;
int no_of_sections=0;
char name[18];
int type=0,offset=0,sizee=0;
size=read(fd,magic,4);
magic[4]=0;
if(size==-1) printf("Eroare");
//printf("%s\n",magic);
if(strcmp(magic,"ct9x")!=0)
{
printf("ERROR\nwrong magic\n");
return;
}
size=read(fd,&heade_size,2);
size=read(fd,&version,2);
//printf("%d\n",version);
if(version<57 || version>92) 
{
printf("ERROR\nwrong version\n");
return;
}
size=read(fd,&no_of_sections,1);
if(no_of_sections<3 || no_of_sections>15) 
{
 printf("ERROR\nwrong sect_nr\n");
return;   
}
else{

if (decizie==0)
{
printf("SUCCESS\n");
printf("version=%d\n",version);
printf("nr_sections=%d\n",no_of_sections);
for (int i=1; i<=no_of_sections; i++)
{
    size=read(fd,name,18);
    size=read(fd,&type,4);
    //printf("section%d: %s %d ",i,name,type);
    if((type!=85) && (type!=70) && (type!=39) && (type!=92))
    {
       printf("ERROR\nwrong sect_types\n");
       return;
    }
    size=read(fd,&offset,4);
    size=read(fd,&sizee,4);
    printf("section%d: %s %d %d\n",i,name,type,sizee);
    //size=read(fd,sfarsit,1);
}
}
else 
{
 for (int i=1; i<=no_of_sections; i++)
{
    size=read(fd,name,18);
    size=read(fd,&type,4);
    //printf("section%d: %s %d ",i,name,type);
    if((type!=85) && (type!=70) && (type!=39) && (type!=92))
    {
       printf("ERROR\nwrong type");
       return;
    }
    size=read(fd,&offset,4);
    size=read(fd,&sizee,4);   
}
}

close(fd);
}
}
}

void fisiereleLinie(const char* path, int sectiune, int linie)
{

int fd = -1;

fd = open(path, O_RDONLY);
if(fd == -1) {
perror("ERROR\ninvalid file");
return;
}
else
{
ssize_t size=0;
char magic[5];
char sfarsit1;
char sfarsit2;
char c;
int nr=1; 
int heade_size=0;
int version=0;
int no_of_sections=0;
char name[18];
int type=0,offset=0,sizee=0;
size=read(fd,magic,4);
magic[4]=0;
if(size==-1) printf("Eroare");
//printf("%s\n",magic);
if(strcmp(magic,"ct9x")!=0)
{
printf("ERROR\ninvalid file");
return;
}
size=read(fd,&heade_size,2);
size=read(fd,&version,2);
//printf("%d\n",version);
if(version<57 || version>92) 
{
printf("ERROR\ninvalid file");
return;
}
size=read(fd,&no_of_sections,1);
if((no_of_sections<3 || no_of_sections>15) || no_of_sections<sectiune) 
{
 printf("ERROR\ninvalid section");
return;   
}
else{
for (int i=1; i<sectiune; i++)
{
    size=read(fd,name,18);
    size=read(fd,&type,4);
    //printf("section%d: %s %d ",i,name,type);
    if((type!=85) && (type!=70) && (type!=39) && (type!=92))
    {
       printf("ERROR\nwrong type");
       return;
    }
    size=read(fd,&offset,4);
    size=read(fd,&sizee,4);
    //printf("section%d: %s %d %d\n",i,name,type,sizee);
    //size=read(fd,sfarsit,1);
}
}
lseek(fd,22,SEEK_CUR);
read(fd,&offset,4);
//read(fd,&size,4);
lseek(fd,offset,SEEK_SET);
read(fd,&sfarsit1,1);
read(fd,&sfarsit2,1);
printf("SUCCESS\n");

while(nr<linie)
{
read(fd,&c,1);
//sfarsit1=sfarsit2;
sfarsit1=c;
//printf("%d",nr);
if (sfarsit1=='\n')
nr++;
}
//printf("\n");

do
{
read(fd,&c,1);
//sfarsit1=sfarsit2;
sfarsit1=c;
printf("%c",c);
}while(c!='\n');

close(fd);
}
}


void listRecFindAll(const char *path)
{
DIR *dir = NULL;
int no_of_sections=0;
int size=0;
int ok=1;
struct dirent *entry = NULL;
char fullPath[1024];
struct stat statbuf;
dir = opendir(path);
if(dir == NULL) {
printf("ERROR\ninvalid directory path");
return;
}
while((entry = readdir(dir)) != NULL) {
    if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) 
    {
        snprintf(fullPath, 1024, "%s/%s", path, entry->d_name);
        if(lstat(fullPath, &statbuf) == 0) {
        //printf("%s\n", fullPath);
        if(S_ISREG(statbuf.st_mode)) 
        {
            int fd = -1;

            fd = open(fullPath, O_RDONLY);
            if(fd == -1) {
            printf("ERROR\ninvalid file");
            return;
        }
        else
        {
            ok=1;
            lseek(fd,8,SEEK_SET);
            read(fd,&no_of_sections,1);
            ///printf("%d\n",no_of_sections);
            for (int i=1; i<=no_of_sections; i++)
            {
                lseek(fd,26,SEEK_CUR);
                read(fd,&size,4);
               // printf("%d\n",size);
                if (size>995) ok=0;
            }

        }
        }
        if(ok==1)
        listRec(fullPath);
            }
        }
    
}
closedir(dir);
}




int main(int argc, char **argv){

        const char s[2] = "=";
    	if(argc >= 2){

        if(strcmp(argv[1], "variant") == 0){
            printf("69823\n");
        return 0;
        }

      if(argc==3){
       for(int i=1; i<argc-1; i++)
        {
            if(strcmp(argv[i],"list")==0)
            {
            char* st=argv[i+1]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s);
            listDir(pat); 
            return 0;
            }
        }
    }


       if(argc==4){
                if((strcmp(argv[2],"recursive")==0) && (strcmp(argv[1],"list")==0))
                {
                char* st=argv[3]; 
                char* pat=strtok(st,s);
                pat = strtok(NULL, s);
                printf("SUCCESS\n");
                listRec(pat); 
                return 0;  
                }

        }

        if(argc==4){
            if((strcmp(argv[1],"list")==0) && (strcmp(argv[2],"has_perm_write")!=0))
            {
            char* st=argv[2]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s); 
            char* st1=argv[3]; 
            char* pat1=strtok(st1,s);
            pat1 = strtok(NULL, s);
            listDirST(pat1,pat);
            return 0;

            }
        }
        

        if (argc==4)
        {
            if((strcmp(argv[1],"list")==0) && (strcmp(argv[2],"has_perm_write")==0))
            {
            char* st=argv[3]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s); 
            printf("SUCCESS\n");
            hasPermision(pat);
            return 0;  
            }
        
        }

          if (argc==3)
        {
            if(strcmp(argv[1],"parse")==0)
            {
            char* st=argv[2]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s); 
            fisierele(pat,0);
            return 0;  
            }
         } 

         if(argc==5)
         {
            if(strcmp(argv[1],"extract")==0)
            {
                char*st=argv[2];
                char* pat=strtok(st,s);
                pat=strtok(NULL,s);
                char*st1=argv[3];
                char*pat1=strtok(st1,s);
                pat1=strtok(NULL,s);
                int a=atoi(pat1);
                char*st2=argv[4];
                char*pat2=strtok(st2,s);
                pat2=strtok(NULL,s);
                int b=atoi(pat2);
                fisiereleLinie(pat,a,b);
                return 0;
            }
         }

         if (argc==3)
        {
            if((strcmp(argv[1],"findall")==0))
            {
            char* st=argv[2]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s); 
            printf("SUCCESS\n");
            listRecFindAll(pat);
            return 0;  
            }
        
        }

        if(argc==3)
        {
         if((strcmp(argv[1],"parse")==0))
            {
            char* st=argv[2]; 
            char* pat=strtok(st,s);
            pat = strtok(NULL, s); 
            fisierele(pat,1);
            return 0;  
            }   
        }

           



	return 0;
    }
}


